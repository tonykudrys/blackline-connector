﻿using Scribe.Core.ConnectorApi;
using Scribe.Core.ConnectorApi.Actions;
using Scribe.Core.ConnectorApi.ConnectionUI;
using Scribe.Core.ConnectorApi.Cryptography;
using Scribe.Core.ConnectorApi.Exceptions;
using Scribe.Core.ConnectorApi.Logger;
using Scribe.Core.ConnectorApi.Query;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Data;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.Serialization;
using Scribe.Core.ConnectorApi.Common;
using ArmaninoObjModel;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Web;
using Newtonsoft.Json.Linq;
using ArmaninoAPI;
using CsvHelper;

namespace Armanino.Connector.BlackLine
{

    //attributes
    [ScribeConnector(
        ConnectorSettings.ConnectorTypeId,
        ConnectorSettings.Name,
        ConnectorSettings.Description,
        typeof(Connector),
        StandardConnectorSettings.SettingsUITypeName,
        StandardConnectorSettings.SettingsUIVersion,
        StandardConnectorSettings.ConnectionUITypeName,
        StandardConnectorSettings.ConnectionUIVersion,
        StandardConnectorSettings.XapFileName,
        
        // roles 
        new[] { "Scribe.IS.Source", "Scribe.IS.Target", "Scribe.IS2.Source", "Scribe.IS2.Target" },
        ConnectorSettings.SupportsCloud, ConnectorSettings.ConnectorVersion)]

    public class Connector : IConnector, ISupportProcessNotifications
    {
        //class level vars----------------------------------------
       
        public bool IsConnected { get; set; }

        private Dictionary<string,DataEntity> dedic = new Dictionary<string,DataEntity>();

        private MetadataProvider metadataProvider = new MetadataProvider();

        private ArmaninoAPI.SFTP sFTP = null;

        public Guid ConnectorTypeId
        {
            get { return new Guid(ConnectorSettings.ConnectorTypeId); }
        }

        private static string CryptoKey
        {
            //Always put a new GUID here
            get { return "87B65BDE-12D9-4D58-8870-6BEAD3333343"; }
        }
        
        //Connect functions----------------------------------------
        public string PreConnect(IDictionary<string, string> properties)
        {
            ConnectorSettings connectorSettings = new ConnectorSettings();
            return connectorSettings.getFormFields(CryptoKey);
        }

        public void Connect(IDictionary<string, string> properties)
        {            
            try
            {
                ConnectorSettings connectorSettings = new ConnectorSettings();
                this.IsConnected = connectorSettings.testConnection(properties, CryptoKey);
            }
            catch (WebException e)
            {
                this.IsConnected = false;
                throw new WebException("WebException Raised While Trying To Connect To BlackLine. Please Verify That The credentials are Valid. (The following error occured : "+e.Message+")");
            }
            catch (Exception e)
            {
                this.IsConnected = false;
                throw new WebException("WebException Raised While Trying To Connect To BlackLine. The following error occured : " + e.Message);
            }
        }
        
        public void Disconnect()
        {
            this.IsConnected = false;
        }

        public void ProcessStarted(Guid ProcID)
        {

        }

        public void ProcessEnded(Guid ProcID, bool t)
        {

        }

        //Request Handelers----------------------------------------
        public IEnumerable<DataEntity> ExecuteQuery(Query query)
        {
            List<DataEntity> dataEntities = new List<DataEntity>(); //container to hold data entities

            if (sFTP == null || !sFTP.test())
            {
                if (ArmaninoAPI.credentials.properties["Password"] != "")
                    sFTP = new ArmaninoAPI.SFTP(ArmaninoAPI.credentials.properties["URL"], 22, ArmaninoAPI.credentials.properties["UserName"], ArmaninoAPI.credentials.properties["Password"]);
                else
                    sFTP = new ArmaninoAPI.SFTP(ArmaninoAPI.credentials.properties["URL"], 22, ArmaninoAPI.credentials.properties["UserName"], "", ArmaninoAPI.credentials.properties["PubKey"]);
            }

            string delimiter;

            if (ArmaninoAPI.credentials.properties["Delimiter"] == "Tab")
                delimiter = '\t'.ToString();
            else
                delimiter = ",";

            String fileNameTemp = ArmaninoAPI.credentials.properties["FileName"].Trim() == "" ? "*" : ArmaninoAPI.credentials.properties["FileName"];
            Dictionary<String, String> Files = sFTP.readFiles(ArmaninoAPI.credentials.properties["DirectoryPath"], fileNameTemp);



            foreach (var f in Files)
            {

                using (TextReader sr = new StringReader(f.Value))
                {
                    CsvReader csv = new CsvReader(sr);

                    if (ArmaninoAPI.credentials.properties["IncludeHeader"] == "True")
                    {
                        csv.Read();
                        csv.ReadHeader();
                    }



                    while (csv.Read())
                    {
                        if (ArmaninoAPI.credentials.properties["FileName"] == "*" || ArmaninoAPI.credentials.properties["FileName"].Trim() == "")
                            fileNameTemp = "sFTP_File";

                        DataEntity dec = new DataEntity();
                        dec.Properties = new EntityProperties();
                        dec.ObjectDefinitionFullName = fileNameTemp;

                        int c = 0;
                        foreach (String Field in ArmaninoAPI.credentials.properties["ColumnHeaders"].Split('|'))
                        {
                            String FieldValue = csv.GetField(c);
                            c++;

                            dec.Properties.Add(Field, FieldValue);
                        }

                        dec.Properties.Add("TargetFileName", f.Key);

                        dataEntities.Add(dec);
                                                
                    }
                }

                if(ArmaninoAPI.credentials.properties["ReadType"] == "True")
                {
                    if (sFTP == null || !sFTP.test())
                    {
                        if (ArmaninoAPI.credentials.properties["Password"] != "")
                            sFTP = new ArmaninoAPI.SFTP(ArmaninoAPI.credentials.properties["URL"], 22, ArmaninoAPI.credentials.properties["UserName"], ArmaninoAPI.credentials.properties["Password"]);
                        else
                            sFTP = new ArmaninoAPI.SFTP(ArmaninoAPI.credentials.properties["URL"], 22, ArmaninoAPI.credentials.properties["UserName"], "", ArmaninoAPI.credentials.properties["PubKey"]);
                    }

                    sFTP.deleteFile(f.Key, ArmaninoAPI.credentials.properties["DirectoryPath"]);
                }
                
            }

            return dataEntities;
                                  
        }

        public OperationResult ExecuteOperation(OperationInput input)
        {
            //results to return to scribe
            var operationResult = new OperationResult();

            // arrays to keep track of the operations statuses
            var operationSuccess = new List<bool>();
            var entitiesAffected = new List<int>();
            var errorList = new List<ErrorResult>();
            var entities = new List<DataEntity>();
            
            var action = input.Name;    //create, update, upsert, delete 
            
            String FileName = ArmaninoAPI.credentials.properties["FileName"];

            string delimiter;
            string headerString = "";

            if (ArmaninoAPI.credentials.properties["Delimiter"] == "Tab")
                delimiter = '\t'.ToString();
            else
                delimiter = ",";

            StringBuilder sb = new StringBuilder();
            List<DataEntity> dataEntities = new List<DataEntity>();

            List<String> sumByFields = new List<string>();
            List<String> groupByFields = new List<string>();

            if(ArmaninoAPI.credentials.properties["SumByFields"].Trim() != "")
                sumByFields = ArmaninoAPI.credentials.properties["SumByFields"].Split('|').ToList();

            if(ArmaninoAPI.credentials.properties["GroupByFields"].Trim() != "")
                groupByFields = ArmaninoAPI.credentials.properties["GroupByFields"].Split('|').ToList();

            DataTable dt = new DataTable();
            dt.Clear();

            foreach (String Field in ArmaninoAPI.credentials.properties["ColumnHeaders"].Split('|'))
            {                
                if (sumByFields.Contains(Field))
                    dt.Columns.Add(Field, typeof(decimal));
                else
                    dt.Columns.Add(Field, typeof(string));

            }

            dt.Columns.Add("GroupByField");
                        
            

            if (ArmaninoAPI.credentials.properties["IncludeHeader"] == "True")
            {
                List<String> tempStrHeader = new List<string>();

                foreach (String Field in ArmaninoAPI.credentials.properties["ColumnHeaders"].Split('|'))
                {
                    tempStrHeader.Add(Field);
                }

                headerString = String.Join(delimiter, tempStrHeader) + Environment.NewLine;
            }

            if (action == "CreateWith")
            {
                //loop through dataentity list
                foreach (DataEntity data in input.Input)
                {
                    //loop through dataentity children list
                    foreach (var childlst in data.Children.Values)
                    {
                        //loop through children values
                        foreach (DataEntity child in childlst)
                        {
                            try
                            {
                                if (child.Properties.ContainsKey("TargetFileName"))
                                {
                                    String FileNameTemp = (from x in child.Properties where x.Key == "TargetFileName" select x.Value).FirstOrDefault().ToString();
                                    if (FileNameTemp != null && FileNameTemp.Trim() != "")
                                        FileName = FileNameTemp;
                                }

                                List<String> tempStr = new List<string>();

                                DataRow _ravi = dt.NewRow();
                                String GroupByValue = "";
                                
                                foreach (String Field in ArmaninoAPI.credentials.properties["ColumnHeaders"].Split('|'))
                                {
                                    String tempPropStr = "";

                                    if (child.Properties.ContainsKey(Field))
                                    {
                                        tempPropStr = (from x in child.Properties where x.Key == Field select x.Value).FirstOrDefault().ToString();

                                        if (tempPropStr.Contains(",") && delimiter == ",")
                                            tempPropStr = "\"" + tempPropStr + "\"";
                                    }

                                    if (groupByFields.Contains(Field)) 
                                        _ravi[Field] = tempPropStr;
                                    else if (sumByFields.Contains(Field))
                                        _ravi[Field] = Decimal.Parse(tempPropStr);

                                    tempStr.Add(tempPropStr);

                                    if (groupByFields.Contains(Field))
                                        GroupByValue += tempPropStr;
                                }

                                _ravi["GroupByField"] = GroupByValue;

                                dt.Rows.Add(_ravi);
                                sb.Append(String.Join(delimiter, tempStr));
                                sb.Append(Environment.NewLine);

                                dataEntities.Add(child);
                            }
                            catch (Exception ex)
                            {
                                var errorResult_err = new ErrorResult
                                {
                                    Description = string.Format("Error inserting/updating record: {0}", ex.Message),
                                    Detail = ex.StackTrace,
                                    Number = 1,
                                };

                                operationSuccess.Add(false);
                                errorList.Add(errorResult_err);
                                entitiesAffected.Add(0);

                                break;
                            }
                        }
                    }

                    var errorResult = new ErrorResult
                    {
                        Number = 0,
                    };
                    errorList.Add(errorResult);
                }
            }
            else
            {
                //loop through dataentity list
                foreach (DataEntity data in input.Input)
                {
                    try
                    {
                        if (data.Properties.ContainsKey("TargetFileName"))
                        {
                            String FileNameTemp = (from x in data.Properties where x.Key == "TargetFileName" select x.Value).FirstOrDefault().ToString();
                            if (FileNameTemp != null && FileNameTemp.Trim() != "")
                                FileName = FileNameTemp;
                        }

                        List<String> tempStr = new List<string>();

                        DataRow _ravi = dt.NewRow();
                        String GroupByValue = "";
                        
                        foreach (String Field in ArmaninoAPI.credentials.properties["ColumnHeaders"].Split('|'))
                        {
                            String tempPropStr = "";

                            if (data.Properties.ContainsKey(Field))
                            {
                                tempPropStr = (from x in data.Properties where x.Key == Field select x.Value).FirstOrDefault().ToString();

                                if (tempPropStr.Contains(",") && delimiter == ",")
                                    tempPropStr = "\"" + tempPropStr + "\"";
                            }

                            if (groupByFields.Contains(Field))
                                _ravi[Field] = tempPropStr;
                            else if (sumByFields.Contains(Field))
                                _ravi[Field] = Decimal.Parse(tempPropStr);

                            tempStr.Add(tempPropStr);

                            if (!groupByFields.Contains(Field))
                                GroupByValue += tempPropStr;
                        }

                        _ravi["GroupByField"] = GroupByValue;

                        dt.Rows.Add(_ravi);
                        sb.Append(String.Join(delimiter, tempStr));
                        sb.Append(Environment.NewLine);

                        dataEntities.Add(data);
                    }
                    catch (Exception ex)
                    {
                        var errorResult_err = new ErrorResult
                        {
                            Description = string.Format("Error inserting/updating record: {0}", ex.Message),
                            Detail = ex.StackTrace,
                            Number = 1,
                        };

                        operationSuccess.Add(false);
                        errorList.Add(errorResult_err);
                        entitiesAffected.Add(0);

                        break;
                    }
                }

                var errorResult = new ErrorResult
                {
                    Number = 0,
                };
                errorList.Add(errorResult);
            }

            //get group by string
            if (groupByFields.Count > 0 && sumByFields.Count > 0)
            {
                sb = getGroupSumBy(dt, groupByFields, sumByFields);                
            }


            if (sFTP == null || !sFTP.test())
            {
                if (ArmaninoAPI.credentials.properties["Password"] != "")
                    sFTP = new ArmaninoAPI.SFTP(ArmaninoAPI.credentials.properties["URL"], 22, ArmaninoAPI.credentials.properties["UserName"], ArmaninoAPI.credentials.properties["Password"]);
                else
                    sFTP = new ArmaninoAPI.SFTP(ArmaninoAPI.credentials.properties["URL"], 22, ArmaninoAPI.credentials.properties["UserName"], "", ArmaninoAPI.credentials.properties["PubKey"]);
            }

            if (ArmaninoAPI.credentials.properties["WriteType"] == "Overwrite")
                sFTP.writeFile(FileName, ArmaninoAPI.credentials.properties["DirectoryPath"], headerString+sb.ToString());
            else
                sFTP.appendFile(FileName, ArmaninoAPI.credentials.properties["DirectoryPath"], sb.ToString(), headerString);

            List<DataEntity> templstDE = new List<DataEntity>();
            DataEntity tempDE = new DataEntity();
            tempDE.Children = new EntityChildren();
            tempDE.Children.Add("c", dataEntities);
            templstDE.Add(tempDE);

            
            operationSuccess.Add(true);
            entitiesAffected.Add(1);

            operationResult.Success = operationSuccess.ToArray();
            operationResult.ObjectsAffected = entitiesAffected.ToArray();
            operationResult.ErrorInfo = errorList.ToArray();
            operationResult.Output = templstDE.ToArray();


            return operationResult;
        }
        
        public MethodResult ExecuteMethod(MethodInput input)
        {
            throw new NotImplementedException();
        }



        public IMetadataProvider GetMetadataProvider()
        {
            return this.metadataProvider;
        }

        static string GetVariableName<T>(Expression<Func<T>> expr)
        {
            var body = (MemberExpression)expr.Body;

            return body.Member.Name;
        }
       
        

        private StringBuilder getGroupSumBy(DataTable dt, List<String> groupByFields, List<String> sumByFields)
        {

            StringBuilder sb = new StringBuilder();

            string delimiter;

            if (ArmaninoAPI.credentials.properties["Delimiter"] == "Tab")
                delimiter = '\t'.ToString();
            else
                delimiter = ",";


            var newDt = dt.AsEnumerable()
              .GroupBy(r => r.Field<String>("GroupByField"))
              .Select(g =>
              {
                  var row = dt.NewRow();

                  foreach (var c in dt.Columns)
                  {
                      if (c.ToString() != "GroupByField")
                      {
                          if (groupByFields.Contains(c.ToString()))
                          {

                              String tempPropStr = g.First().Field<String>(c.ToString());

                              if (tempPropStr.Contains(",") && delimiter == ",")
                                  tempPropStr = "\"" + tempPropStr + "\"";

                              row[c.ToString()] = tempPropStr;

                          }
                          else if (sumByFields.Contains(c.ToString()))
                          {
                              row[c.ToString()] = g.Sum(r => r.Field<Decimal>(c.ToString()));
                          }
                          else
                          {
                              row[c.ToString()] = "";
                          }
                      }
                  }

                  return row;
              }).CopyToDataTable();


            return ExportDataTable(newDt);



            List<string> distinctGs = (from x in dt.AsEnumerable() select x.Field<String>("GroupByField")).Distinct().ToList();

            foreach(string G in distinctGs)
            {
                List<String> tempStr = new List<string>();
                var tempData = (from x in dt.AsEnumerable() where x.Field<String>("GroupByField") == G select x);


                foreach(var c in dt.Columns)
                {
                    if(c.ToString() != "GroupByField")
                    {
                        String tempPropStr = "";

                        if (groupByFields.Contains(c.ToString()))
                        {

                            tempPropStr = tempData.First().Field<String>(c.ToString());

                            if (tempPropStr.Contains(",") && delimiter == ",")
                                tempPropStr = "\"" + tempPropStr + "\"";
                
                        }
                        else if(sumByFields.Contains(c.ToString()))
                        {
                            Decimal d = (from x in tempData select decimal.Parse(x.Field<String>(c.ToString()))).Sum();
                            tempPropStr = d.ToString();
                        }
                        else
                        {
                            tempPropStr = "";
                        }

                        tempStr.Add(tempPropStr);


                    }


                }

                sb.Append(String.Join(delimiter, tempStr));
                sb.Append(Environment.NewLine);
            }

            return sb;
        }

        private StringBuilder ExportDataTable(DataTable dt)
        {
            string delimiter;

            if (ArmaninoAPI.credentials.properties["Delimiter"] == "Tab")
                delimiter = '\t'.ToString();
            else
                delimiter = ",";

            StringBuilder writer = new StringBuilder();

            foreach (DataRow row in dt.AsEnumerable())
            {
                writer.AppendLine(string.Join(delimiter, row.ItemArray.Select(x => x.ToString())));
            }
            return writer;

        }

        /// <summary>
        /// Adds user-requested filters
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entities"></param>
        /// <param name="query"></param>
        /// <returns></returns>
        /*private IEnumerable<T> ApplyFilters<T>(IQueryable<T> entities, Query query)
        {

            //Use Microsoft's System.Linq.Dynamic library to select the items we want IF there are any filters in the query:
            var results = (query.Constraints != null) ? entities.Where(query.ToLinqExpression()) : entities;

            //order the results if the user has requested it, then send it back:
            return (query.RootEntity.SequenceList.Count > 0) ? results.OrderBy(query.ToOrderByLinqExpression()) : results;

        }*/
    }
}
