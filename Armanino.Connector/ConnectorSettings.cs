﻿//using Scribe.Core.Common.Cryptography;
using Scribe.Core.ConnectorApi.Cryptography;
using Scribe.Core.ConnectorApi.ConnectionUI;
using System;
using System.Collections.Generic;
using System.Net;

namespace Armanino.Connector
{
    public class ConnectorSettings
    {
        public const string ConnectorTypeId = "{96837794-5F7D-4EE2-85B0-53AF3F5E603E}";
        public const string ConnectorVersion = "1.0";
        public const string Description = "AMLLP BlackLine Connector";
        public const string Name = "AmllpBlackLineConnector";
        public const bool SupportsCloud = true;

        public string getFormFields(String CryptoKey)
        {
            FormDefinition formDefinition = new FormDefinition();
            formDefinition.CompanyName = "Armanino, LLP.";
            formDefinition.CryptoKey = CryptoKey;
            int c = 0;

            formDefinition.Add(new EntryDefinition()
            {
                InputType = InputType.Text,
                IsRequired = true,
                Label = "Column Headers",
                PropertyName = "ColumnHeaders",
                Order = c++
            });

            formDefinition.Add(new EntryDefinition()
            {
                InputType = InputType.Text,
                IsRequired = false,
                Label = "Group By Fields",
                PropertyName = "GroupByFields",
                Order = c++
            });

            formDefinition.Add(new EntryDefinition()
            {
                InputType = InputType.Text,
                IsRequired = false,
                Label = "Sum By Fields",
                PropertyName = "SumByFields",
                Order = c++
            });

            formDefinition.Add(new EntryDefinition()
            {
                InputType = InputType.Text,
                IsRequired = true,
                Label = "SFTP URL",
                PropertyName = "URL",
                Order = c++
            });

            formDefinition.Add(new EntryDefinition()
            {
                InputType = InputType.Text,
                IsRequired = true,
                Label = "User Name",
                PropertyName = "UserName",
                Order = c++
            });

            formDefinition.Add(new EntryDefinition()
            {
                InputType = InputType.Password,
                IsRequired = false,
                Label = "Password",
                PropertyName = "Password",
                Order = c++
            });

            formDefinition.Add(new EntryDefinition()
            {
                InputType = InputType.Password,
                IsRequired = false,
                Label = "Private Key",
                PropertyName = "PubKey",
                Order = c++
            });

            formDefinition.Add(new EntryDefinition()
            {
                InputType = InputType.Text,
                IsRequired = true,
                Label = "Remote Directory Path",
                PropertyName = "DirectoryPath",
                Order = c++
            });

            formDefinition.Add(new EntryDefinition()
            {
                InputType = InputType.Text,
                IsRequired = false,
                Label = "File Name",
                PropertyName = "FileName",
                Order = c++
            });

            EntryDefinition Delimiter = new EntryDefinition();

            Delimiter.InputType = InputType.Text;
            Delimiter.IsRequired = true;
            Delimiter.Label = "Delimiter";
            Delimiter.PropertyName = "Delimiter";

            //set the position in which the control will be displayed
            Delimiter.Order = c++;

            //the options will hold the list of values to be placed in the combo box
            //The first value is what will be displayed to the user
            //The second value is the actual value that 
            //will be passed back to the connector
            Delimiter.Options.Add("Tab", "Tab");
            Delimiter.Options.Add("Comma", "Comma");
            formDefinition.Add(Delimiter);

            EntryDefinition IncludeHeader = new EntryDefinition();

            IncludeHeader.InputType = InputType.Text;
            IncludeHeader.IsRequired = true;
            IncludeHeader.Label = "Include Header Row";
            IncludeHeader.PropertyName = "IncludeHeader";

            //set the position in which the control will be displayed
            IncludeHeader.Order = c++;

            //the options will hold the list of values to be placed in the combo box
            //The first value is what will be displayed to the user
            //The second value is the actual value that 
            //will be passed back to the connector
            IncludeHeader.Options.Add("False", "False");
            IncludeHeader.Options.Add("True", "True");
            formDefinition.Add(IncludeHeader);

            EntryDefinition WriteType = new EntryDefinition();

            WriteType.InputType = InputType.Text;
            WriteType.IsRequired = false;
            WriteType.Label = "Overwrite or Append";
            WriteType.PropertyName = "WriteType";

            //set the position in which the control will be displayed
            WriteType.Order = c++;

            //the options will hold the list of values to be placed in the combo box
            //The first value is what will be displayed to the user
            //The second value is the actual value that 
            //will be passed back to the connector
            WriteType.Options.Add("Overwrite", "Overwrite");
            WriteType.Options.Add("Append", "Append");
            formDefinition.Add(WriteType);

            EntryDefinition ReadType = new EntryDefinition();

            ReadType.InputType = InputType.Text;
            ReadType.IsRequired = false;
            ReadType.Label = "Delete File After Read";
            ReadType.PropertyName = "ReadType";

            //set the position in which the control will be displayed
            ReadType.Order = c++;

            //the options will hold the list of values to be placed in the combo box
            //The first value is what will be displayed to the user
            //The second value is the actual value that 
            //will be passed back to the connector
            ReadType.Options.Add("False", "False");
            ReadType.Options.Add("True", "True");
            formDefinition.Add(ReadType);


            return formDefinition.Serialize();
        }

        public bool testConnection(IDictionary<string, string> properties, String CryptoKey)
        {
            ArmaninoAPI.SFTP sFTP = null;
            ArmaninoAPI.credentials.properties = properties;
            ArmaninoAPI.credentials.properties["Password"] = Decryptor.Decrypt_AesManaged(properties["Password"], CryptoKey);
            ArmaninoAPI.credentials.properties["PubKey"] = Decryptor.Decrypt_AesManaged(properties["PubKey"], CryptoKey);

            if(ArmaninoAPI.credentials.properties["Password"] != "")
                sFTP = new ArmaninoAPI.SFTP(properties["URL"], 22, properties["UserName"], ArmaninoAPI.credentials.properties["Password"]);
            else
                sFTP = new ArmaninoAPI.SFTP(properties["URL"], 22, properties["UserName"], "", ArmaninoAPI.credentials.properties["PubKey"]);
             
            if (sFTP.test())
            {
                return true;
            }
            else
            {
                throw new WebException("could not connect to sFTP");
            }
            
        }
    }
}
