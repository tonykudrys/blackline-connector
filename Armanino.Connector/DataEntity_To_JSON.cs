﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Scribe.Core.ConnectorApi.Metadata;
using Scribe.Core.ConnectorApi;

namespace Armanino.Connector
{
    class DataEntity_To_JSON
    {
        public JObject EntityToJSON(DataEntity dataEntity)
        {
            JObject jdoc = new JObject();
            jdoc.Add("entity", dataEntity.ObjectDefinitionFullName);

            MetadataProvider metadataProvider = new MetadataProvider();
            //IObjectDefinition objDef = metadataProvider.RetrieveObjectDefinition(dataEntity.ObjectDefinitionFullName);

            foreach (var props in dataEntity.Properties)
            {
                if (props.Value == null || String.IsNullOrEmpty(props.Value.ToString().Trim()))
                    continue;

                //String t = (from x in objDef.PropertyDefinitions where x.FullName == props.Key select x.PropertyType).FirstOrDefault();

                //DateTime dt = new DateTime();

                try
                {
                    DateTime dt = (DateTime)props.Value;
                    jdoc.Add(props.Key, JToken.FromObject(dt.ToString("yyyy-MM-dd")));
                }
                catch
                {
                    jdoc.Add(props.Key, JToken.FromObject(props.Value));
                }
                
                /*if (DateTime.TryParse(props.Value, out dt))
                {
                    jdoc.Add(props.Key, JToken.FromObject(dt.ToString("yyyy-MM-dd")));
                }
                else
                {
                    jdoc.Add(props.Key, JToken.FromObject(props.Value));
                }*/
            }

            foreach (var child in dataEntity.Children)
            {
                String ChildName = child.Value.FirstOrDefault().ObjectDefinitionFullName;
                ChildName = ChildName == "BillLineItem" ? "billLineItems" : ChildName;
                JArray jarrayObj = new JArray();

                foreach (DataEntity de in child.Value)
                {
                    JObject jdocChild = EntityToJSON(de);
                    jarrayObj.Add(jdocChild);
                }

                jdoc.Add(ChildName, jarrayObj);
            }

            return jdoc;
        }

        private string getChildName(String name)
        {
            switch (name)
            {
                case "BillLineItem":
                    return "billLineItems";   
                default:
                    return name;
            }
        }
    }
}
