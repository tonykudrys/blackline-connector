﻿using Scribe.Core.ConnectorApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Armanino.Connector
{
    class DataEntity_To_Object
    {
        private T EntityToObject<T>(DataEntity scribeEntity, T restEntity)
        {

            //get the list of fields from the object. Extract into a Dictionary.
            var fieldInfo = restEntity.GetType().GetProperties().ToDictionary(key => key.Name, value => value);

            Dictionary<String, String> objInfo = this.get_property_Dictonary(fieldInfo);

            //set object primitive property type values from DataEntity
            restEntity = this.set_Properties(scribeEntity, restEntity, fieldInfo);
            
            //loop through children of DataEntity and set as target object properties
            foreach (KeyValuePair<string, List<DataEntity>> c in scribeEntity.Children)
            {
                if (c.Value.Count() < 1)
                    continue;

                String key = c.Value[0].ObjectDefinitionFullName;
                String objName;

                
                PropertyInfo pi = restEntity.GetType().GetProperty(key);

                if (restEntity.GetType().GetProperty(key) == null)
                {
                    continue;
                }
                else
                {
                    objName = "coupaAPI_obj." + objInfo[key].Replace("[", "").Replace("]", "") + ",coupaAPI";
                }

                //TODO look into using type from pi instead
                Type t = Type.GetType(objName);
                t = pi.PropertyType;

                if (t == null)
                    continue; //possible exception or primative type

                var childobj = FormatterServices.GetUninitializedObject(t);

                if (objInfo.ContainsKey(key))
                {
                    DataEntity de = new DataEntity();
                    de.ObjectDefinitionFullName = key;

                    foreach (DataEntity DE in scribeEntity.Children[c.Key])
                    {
                        List<DataEntity> lst = new List<DataEntity>();
                        de.Children = DE.Children;
                        de.Properties = DE.Properties;
                    }

                    childobj = this.EntityToObject(de, childobj);

                    fieldInfo[key].SetValue(restEntity, childobj, null);
                }
            }

            return restEntity;
        }

        private Dictionary<String, String> get_property_Dictonary(Dictionary<string, PropertyInfo> fieldInfo)
        {
            Dictionary<String, String> objInfo = new Dictionary<String, String>();

            foreach (KeyValuePair<String, PropertyInfo> f in fieldInfo)
            {
                String TypeName = f.Value.PropertyType.ToString();

                if (TypeName.Contains("coupaAPI_obj"))
                    TypeName = TypeName + ",coupaAPI";

                Type t = Type.GetType(TypeName);

                if (t.IsArray)
                {
                    t = t.GetElementType();
                }

                objInfo.Add(f.Key, t.Name);
            }

            return objInfo;
        }

        private T set_Properties<T>(DataEntity scribeEntity, T restEntity, Dictionary<string, PropertyInfo> fieldInfo)
        {
            //create a matching set of key value pairs
            var matchingFieldValues = scribeEntity.Properties.Where(field => fieldInfo.ContainsKey(field.Key));

            //Loop through the field dictionary and assign the values from dataEntity to the target object
            foreach (var field in matchingFieldValues)
            {
                fieldInfo[field.Key].SetValue(restEntity, Convert.ChangeType(field.Value, fieldInfo[field.Key].PropertyType), null);
            }

            return restEntity;
        }
    }
}
