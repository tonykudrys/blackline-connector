﻿using Scribe.Core.ConnectorApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Armanino.Connector
{
    class DataEntity_To_XML
    {
        public String EntityToXML(DataEntity dataEntity)
        {
            XDocument xdoc = new XDocument();
            XElement root = new XElement(dataEntity.ObjectDefinitionFullName);
            xdoc.Add(root);

            XElement entityEL = new XElement("entity", dataEntity.ObjectDefinitionFullName);
            xdoc.Element(dataEntity.ObjectDefinitionFullName).Add(entityEL);

            foreach (var props in dataEntity.Properties)
            {
                if (props.Value == null || String.IsNullOrEmpty(props.Value.ToString().Trim()))
                    continue;

                XElement propEL = new XElement(props.Key, props.Value);
                xdoc.Element(dataEntity.ObjectDefinitionFullName).Add(propEL);
            }

            foreach (var child in dataEntity.Children)
            {
                String ChildName = child.Value.FirstOrDefault().ObjectDefinitionFullName;
                foreach (DataEntity de in child.Value)
                {
                    
                    XElement childEL = XElement.Parse(EntityToXML(de));
                    xdoc.Element(dataEntity.ObjectDefinitionFullName).Add(childEL);
                }
            }

            return xdoc.ToString();
        }

    }
}
