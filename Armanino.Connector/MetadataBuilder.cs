﻿using System;
using System.Collections.Generic;
using System.Linq;
using Scribe.Core.ConnectorApi;
using Scribe.Core.ConnectorApi.Metadata;
using System.Collections;
using System.Reflection;
using System.IO;
using System.Xml.Linq;
using System.Xml.Serialization;
using ArmaninoObjModel;

namespace Armanino.Connector
{
    class MetadataBuilder
    {
        private List<string> supportedActionsTopLvlElements = new List<string> { "Query", "Update", "Insert" };
        private List<string> supportedActionsLowLvlElements = new List<string> { "UpdateWith", "CreateWith" };

        public IEnumerable<IActionDefinition> GetActionDefinitions()
        {
            return new List<IActionDefinition>
                   {
                       new ActionDefinition
                       {
                           Description = "Queries",
                           FullName = "Query",
                           KnownActionType = KnownActions.Query,
                           Name = "Query",
                           SupportsBulk = false,
                           SupportsConstraints = true,
                           SupportsInput = false,
                           SupportsLookupConditions = true,
                           SupportsMultipleRecordOperations = false,
                           SupportsRelations = true,
                           SupportsSequences = false
                       },
                       /*new ActionDefinition
                       {
                           Description = "Update",
                           FullName = "Update",
                           KnownActionType = KnownActions.Update,
                           Name = "Update",
                           SupportsBulk = false,
                           SupportsConstraints = false,
                           SupportsInput = true,
                           SupportsLookupConditions = true,
                           SupportsMultipleRecordOperations = false,
                           SupportsRelations = false,
                           SupportsSequences = false
                       },*/
                       new ActionDefinition
                       {
                           Description = "Create With",
                           FullName = "CreateWith",
                           KnownActionType = KnownActions.CreateWith,
                           Name = "CreateWith",
                           SupportsBulk = false,
                           SupportsConstraints = false,
                           SupportsInput = true,
                           SupportsLookupConditions = true,
                           SupportsMultipleRecordOperations = false,
                           SupportsRelations = true,
                           SupportsSequences = false
                       },/*
                       new ActionDefinition
                       {
                           Description = "Update With",
                           FullName = "UpdateWith",
                           KnownActionType = KnownActions.UpdateWith,
                           Name = "UpdateWith",
                           SupportsBulk = false,
                           SupportsConstraints = false,
                           SupportsInput = true,
                           SupportsLookupConditions = true,
                           SupportsMultipleRecordOperations = false,
                           SupportsRelations = true,
                           SupportsSequences = false
                       },*/
                       new ActionDefinition
                       {
                           Description = "Create",
                           FullName = "Create",
                           KnownActionType = KnownActions.Create,
                           Name = "Create",
                           SupportsBulk = false,
                           SupportsConstraints = false,
                           SupportsInput = true,
                           SupportsLookupConditions = true,
                           SupportsMultipleRecordOperations = false,
                           SupportsRelations = false,
                           SupportsSequences = false
                       }
                   };
        }

        public IEnumerable<IObjectDefinition> GetObjectDefinitions()
        {
            List<IObjectDefinition> MetaDataObject = new List<IObjectDefinition>();
            xml_to_obj x = new xml_to_obj();
            //List<con_objects> cobjs = x.getTypeDic();
            obj_hold oh = new obj_hold();

            ArmaninoAPI.MetaData apiMeta = new ArmaninoAPI.MetaData();
            MetaDataObject.AddRange(apiMeta.getMetaData());

            Metadata_Custom apiMetaCustom = new Metadata_Custom();
            MetaDataObject.AddRange(apiMetaCustom.GetCustomObjectDefinitions());

            List<IObjectDefinition> uniqueList = new List<IObjectDefinition>();

            var namelist = (from o in MetaDataObject orderby o.FullName select o.FullName).Distinct();

            foreach (string name in namelist)
            {
                var ob = (from o in MetaDataObject
                          where o.FullName == name
                          select o).FirstOrDefault();
                if (ob == null)
                    continue;

                uniqueList.Add(ob);
            }

            return uniqueList;
        }

        public List<IObjectDefinition> getDataStruc(Type t, string name, List<string> supportedActions, bool query = true)
        {
            List<IObjectDefinition> objList = new List<IObjectDefinition>();    // list of object definitions
            List<IPropertyDefinition> PropertyDefinitions = new List<IPropertyDefinition>();    //list of property definitions
            List<IRelationshipDefinition> re = new List<IRelationshipDefinition>(); //list of related objects

            //get the property types of the currnt object
            PropertyInfo[] propertyInfos;
            propertyInfos = t.GetProperties();

            //array of base or primative types. properties of these types will be setup as the properties in the object definition
            String[] types = new String[]{typeof(string).Name, typeof(int).Name, typeof(decimal).Name, typeof(double).Name, typeof(bool).Name, typeof(Boolean).Name,
                 typeof(DateTime).Name, typeof(Decimal).Name, typeof(Double).Name, typeof(string).Name, typeof(Single).Name, typeof(object).Name, typeof(Object).Name, typeof(Int64).Name};

            try
            {
                //loop through each property
                foreach (var ifield in propertyInfos)
                {
                    // if type is primative or base add it to the property definition list
                    if (types.Contains(ifield.PropertyType.Name))
                    {

                        // add new property to property definition list
                        PropertyDefinitions.Add(
                                            new PropertyDefinition
                                            {
                                                Description = ifield.Name,
                                                FullName = ifield.Name,
                                                IsPrimaryKey = false,
                                                Name = ifield.Name,
                                                Nullable = true,
                                                PresentationType = ifield.PropertyType.Name,
                                                PropertyType = ifield.PropertyType.Name,
                                                UsedInActionInput = query,
                                                UsedInLookupCondition = query,
                                                UsedInQueryConstraint = query,
                                                UsedInActionOutput = query,
                                                UsedInQuerySelect = query,
                                                UsedInQuerySequence = query,

                                            });
                    }
                    else  //the property is an object. setup the object definition (recur) & setup the relationship with the current object
                    {
                        

                        Boolean isArray = false;
                        // Check if property is an array.  if it is send the element type.
                        if (ifield.PropertyType.IsArray)
                        {
                            //is array
                            objList.AddRange(getDataStruc(ifield.PropertyType.GetElementType(), ifield.PropertyType.Name, supportedActionsLowLvlElements));
                            isArray = true;


                        }
                        else
                        {
                            //is not array
                            objList.AddRange(getDataStruc(ifield.PropertyType, ifield.PropertyType.Name, supportedActionsLowLvlElements));
                            isArray = false;
                        }

                        PropertyDefinitions.Add(
                        new PropertyDefinition
                        {
                            Description = ifield.Name,
                            FullName = ifield.Name,
                            IsPrimaryKey = false,
                            MaxOccurs = isArray ? -1 : 1,
                            Name = ifield.Name.Replace("[]", ""),
                            Nullable = true,
                            PresentationType = ifield.PropertyType.Name.Replace("[]", ""),
                            PropertyType = ifield.PropertyType.Name.Replace("[]", ""),
                            UsedInActionInput = query,
                            UsedInLookupCondition = query,
                            UsedInQueryConstraint = query,
                            UsedInActionOutput = query,
                            UsedInQuerySelect = query,
                            UsedInQuerySequence = query,

                        });

                    }
                }


                //add the object definition to the object definition list
                objList.Add(new ObjectDefinition
                {
                    FullName = name,
                    Description = "",
                    Hidden = false,
                    Name = name,
                    SupportedActionFullNames = supportedActions, //new List<string> { "Query", "Update", "UpdateWith", "Insert", "CreateWith" },
                    PropertyDefinitions = PropertyDefinitions,
                    RelationshipDefinitions = re,
                });

            }
            catch (Exception ex)
            {
                
            }

            return objList;
        }

    }
}
