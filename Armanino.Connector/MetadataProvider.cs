﻿using System;
using System.Collections.Generic;
using System.Linq;
using Scribe.Core.ConnectorApi;
using Scribe.Core.ConnectorApi.Metadata;
using System.Collections;
using System.Reflection;
using System.IO;
using System.Xml.Linq;
using System.Xml.Serialization;
using ArmaninoObjModel;
namespace Armanino.Connector
{
    class MetadataProvider : IMetadataProvider
    {
        private MetadataBuilder metadataBuilder = new MetadataBuilder();
                
        public IEnumerable<IActionDefinition> RetrieveActionDefinitions()
        {
            return metadataBuilder.GetActionDefinitions();
        }

        public IMethodDefinition RetrieveMethodDefinition(string objectName, bool shouldGetParameters = false)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IMethodDefinition> RetrieveMethodDefinitions(bool shouldGetParameters = false)
        {
            throw new NotImplementedException();
        }

        public IObjectDefinition RetrieveObjectDefinition(string objectName, bool shouldGetProperties = false, bool shouldGetRelations = false)
        {
            return metadataBuilder.GetObjectDefinitions().First(n => n.Name == objectName);
        }

        public IEnumerable<IObjectDefinition> RetrieveObjectDefinitions(bool shouldGetProperties = false, bool shouldGetRelations = false)
        {
            return metadataBuilder.GetObjectDefinitions();
        }

        public void Dispose()
        {
            
        }
        
        public void ResetMetadata()
        {
            
        }
    }
}
