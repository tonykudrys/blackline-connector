﻿using Scribe.Core.ConnectorApi;
using Scribe.Core.ConnectorApi.Metadata;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Armanino.Connector
{
    class Metadata_Custom
    {
        public IEnumerable<IObjectDefinition> GetCustomObjectDefinitions()
        {
            List<IObjectDefinition> MetaDataObject = new List<IObjectDefinition>();

            MetaDataObject.Add(getStringFormater());
            MetaDataObject.AddRange(getColumnHeaders());

            return MetaDataObject;
        }

        private IEnumerable<IObjectDefinition> getColumnHeaders()
        {
            List<IObjectDefinition> MetaDataObject = new List<IObjectDefinition>();
            List<IPropertyDefinition> PropertyDefinitions = new List<IPropertyDefinition>();    //list of property definitions
            List<IRelationshipDefinition> re = new List<IRelationshipDefinition>(); //list of related objects
            String rel_prop_that_str = String.Join(",", ArmaninoAPI.credentials.properties["ColumnHeaders"].Split('|'));

            //create header entity
            List<IPropertyDefinition> PropertyDefinitions_Head = new List<IPropertyDefinition>();    //list of property definitions

            String fileNameTemp = ArmaninoAPI.credentials.properties["FileName"];
            if (ArmaninoAPI.credentials.properties["FileName"] == "*" || ArmaninoAPI.credentials.properties["FileName"].Trim() == "")
                fileNameTemp = "sFTP_File";

            PropertyDefinitions_Head.Add(
                new PropertyDefinition
                {
                    Description = fileNameTemp,
                    FullName = fileNameTemp,
                    IsPrimaryKey = false,
                    Name = fileNameTemp,
                    Nullable = true,
                    MinOccurs = 1,
                    PresentationType = "Object",
                    PropertyType = "Object",
                    UsedInActionInput = true,
                    UsedInLookupCondition = true,
                    UsedInQueryConstraint = true,
                    UsedInActionOutput = true,
                    UsedInQuerySelect = true,
                    UsedInQuerySequence = true,

                });

            IRelationshipDefinition rel = new RelationshipDefinition
            {
                Name = "sFTPHead_" + fileNameTemp,
                Description = "",
                FullName = "sFTPHead_" + fileNameTemp,
                ThisObjectDefinitionFullName = "sFTPHead",
                RelatedObjectDefinitionFullName = fileNameTemp,
                RelationshipType = RelationshipType.PropertyCollection,
                ThisProperties = "",
                RelatedProperties = rel_prop_that_str,
            };

            re.Add(rel);

            MetaDataObject.Add(new ObjectDefinition
            {
                FullName = "sFTPHead",
                Description = "BlackLine sFTP Header",
                Hidden = false,
                Name = "sFTPHead",
                SupportedActionFullNames = new List<string> { "CreateWith"},
                PropertyDefinitions = PropertyDefinitions_Head, 
                RelationshipDefinitions = re,

            });


            foreach (String Field in ArmaninoAPI.credentials.properties["ColumnHeaders"].Split('|'))
            {
                PropertyDefinitions.Add(
                new PropertyDefinition
                {
                    Description = Field,
                    FullName = Field,
                    IsPrimaryKey = false,
                    Name = Field,
                    Nullable = true,
                    MaxOccurs = 1,
                    MinOccurs = 1,
                    PresentationType = "String",
                    PropertyType = "String",
                    UsedInActionInput = true,
                    UsedInLookupCondition = true,
                    UsedInQueryConstraint = true,
                    UsedInActionOutput = true,
                    UsedInQuerySelect = true,
                    UsedInQuerySequence = true, 

                });
            }

            PropertyDefinitions.Add(
            new PropertyDefinition
            {
                Description = "TargetFileName",
                FullName = "TargetFileName",
                IsPrimaryKey = false,
                Name = "TargetFileName",
                Nullable = true,
                MaxOccurs = 1,
                MinOccurs = 1,
                PresentationType = "String",
                PropertyType = "String",
                UsedInActionInput = true,
                UsedInLookupCondition = false,
                UsedInQueryConstraint = false,
                UsedInActionOutput = true,
                UsedInQuerySelect = true,
                UsedInQuerySequence = true,

            });



            MetaDataObject.Add(new ObjectDefinition
            {
                FullName = fileNameTemp,
                Description = "BlackLine sFTP Target File",
                Hidden = false,
                Name = fileNameTemp,
                SupportedActionFullNames = new List<string> { "Create", "Query" },
                PropertyDefinitions = PropertyDefinitions,
                RelationshipDefinitions = new List<IRelationshipDefinition>(),
            });

            //add the object definition to the object definition list
            return MetaDataObject;
        }

        private IObjectDefinition getStringFormater()
        {
            List<IPropertyDefinition> PropertyDefinitions = new List<IPropertyDefinition>();    //list of property definitions
            List<IRelationshipDefinition> re = new List<IRelationshipDefinition>(); //list of related objects

            PropertyDefinitions.Add(
                new PropertyDefinition
                {
                    Description = "unformated_String",
                    FullName = "unformated_String",
                    IsPrimaryKey = false,
                    Name = "unformated_String",
                    Nullable = true,
                    MaxOccurs = 1,
                    MinOccurs = 1,
                    PresentationType = "String",
                    PropertyType = "String",
                    UsedInActionInput = false,
                    UsedInLookupCondition = true,
                    UsedInQueryConstraint = true,
                    UsedInActionOutput = false,
                    UsedInQuerySelect = true,
                    UsedInQuerySequence = true,

                });

            PropertyDefinitions.Add(
                new PropertyDefinition
                {
                    Description = "formated_String",
                    FullName = "formated_String",
                    IsPrimaryKey = false,
                    Name = "formated_String",
                    Nullable = true,
                    MaxOccurs = 1,
                    MinOccurs = 1,
                    PresentationType = "String",
                    PropertyType = "String",
                    UsedInActionInput = false,
                    UsedInLookupCondition = true,
                    UsedInQueryConstraint = true,
                    UsedInActionOutput = true,
                    UsedInQuerySelect = true,
                    UsedInQuerySequence = true,

                });

            PropertyDefinitions.Add(
                new PropertyDefinition
                {
                    Description = "delimiter",
                    FullName = "delimiter",
                    IsPrimaryKey = false,
                    Name = "delimiter",
                    Nullable = true,
                    MaxOccurs = 1,
                    MinOccurs = 1,
                    PresentationType = "String",
                    PropertyType = "String",
                    UsedInActionInput = false,
                    UsedInLookupCondition = true,
                    UsedInQueryConstraint = true,
                    UsedInActionOutput = true,
                    UsedInQuerySelect = true,
                    UsedInQuerySequence = true,

                });

            //add the object definition to the object definition list
            return (new ObjectDefinition
            {
                FullName = "split_String",
                Description = "splits string into data set",
                Hidden = false,
                Name = "split_String",
                SupportedActionFullNames = new List<string> { "Query" },
                PropertyDefinitions = PropertyDefinitions,
                RelationshipDefinitions = re,
            });
        }
    }
}
