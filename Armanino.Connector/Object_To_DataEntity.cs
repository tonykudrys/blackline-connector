﻿using Scribe.Core.ConnectorApi;
using Scribe.Core.ConnectorApi.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Armanino.Connector
{
    class Object_To_DataEntity
    {
        public DataEntity ObjectToEntity<T>(string name, T Data, Type dtype)
        {
            DataEntity dataEnt = new DataEntity(name);  //new base data entity
            dataEnt.Children = new EntityChildren();    //child list in data entity
            EntityProperties entp = new EntityProperties(); //property list for data entity
            
            //if the passed in data is null or empty return empty data entity
            if (Data == null)
            {
                //add the compiled list of entity propeties to the base data entity
                dataEnt.Properties = entp;
                return dataEnt;
            }

            //array of base or primative types. properties of these types will be setup as the properties in the object definition
            String[] types = new String[]{typeof(string).Name, typeof(int).Name, typeof(decimal).Name, typeof(double).Name, typeof(bool).Name, typeof(Boolean).Name,
                 typeof(DateTime).Name, typeof(Decimal).Name, typeof(Double).Name, typeof(string).Name, typeof(Single).Name, typeof(object).Name, typeof(Object).Name, typeof(Int64).Name};

            //get the list of properties for the data type using reflection
            PropertyInfo[] properties = dtype.GetProperties();

            int childCount = 1;

            //loop through the data type properties and fill the base data entity properties and children
            foreach (PropertyInfo property in properties)
            {
                string varName = property.Name; //the property name
                var varValue = Data.GetType().GetProperty(varName).GetValue(Data, null);    //the property value

                //if data type of property is a primative then set it as a property else add it as a child
                if (types.Contains(property.PropertyType.Name))
                {
                    /* *****************************
                     * create data entity properties
                     * *****************************/

                    //null date code to avoid sql date over load exception
                    if (property.PropertyType == typeof(DateTime) && (DateTime)varValue < Convert.ToDateTime("01/01/1800"))
                    {
                        varValue = Convert.ToDateTime("01/01/1800");
                    }

                    //add property and value to the property list
                    entp.Add(varName, varValue);
                }
                else
                {

                    /* *****************************
                     * create data entity children
                     * *****************************/

                    //if property is an array build array of dataentites to be loaded into the child list
                    if (property.PropertyType.IsArray && varValue != null)
                    {
                        //temporary list of data entities to hold all child data entities in array
                        List<DataEntity> tempList = new List<DataEntity>();

                        try
                        {
                            //traverse through the array
                            foreach (var v in (Array)varValue)
                            {
                                //prevent infinate recursion
                                if (name != varName && property.PropertyType != dtype)
                                {
                                    //get the data entity and add it to the temp list
                                    tempList.Add(ObjectToEntity(varName, v, property.PropertyType.GetElementType()));
                                }
                            }

                            //add the templist of child data entities to the children property
                            dataEnt.Children.Add(name + "_" + varName, tempList);

                        }
                        catch (Exception)
                        {
                            //catch all exception
                            throw;
                        }

                    }
                    else //load single data entity into child list
                    {
                        //prevent infinate recursion
                        if (name != varName && property.PropertyType != dtype)
                        {
                            //get the data entity and add it to the children property NOTE 
                            dataEnt.Children.Add(name + "_" + varName, new List<DataEntity>(new[] { ObjectToEntity(varName, varValue, property.PropertyType) }));
                        }
                    }

                    childCount++;
                }

            }


            //add the compiled list of entity propeties to the base data entity
            dataEnt.Properties = entp;

            //return the data entity
            return dataEnt;
        }

    }
}
