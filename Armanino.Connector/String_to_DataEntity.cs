﻿using Scribe.Core.ConnectorApi;
using Scribe.Core.ConnectorApi.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Armanino.Connector
{
    class String_to_DataEntity
    {
        public List<DataEntity> SplitString(String s, char d)
        {
            s = s.TrimEnd(d);
            s = s.TrimStart(d);
            List<DataEntity> dataEntities = new List<DataEntity>();
            List<String> s_lst = s.Split(d).Distinct().ToList();

            foreach(var sf in s_lst)
            {
                DataEntity de = new DataEntity("split_String");
                EntityProperties entp = new EntityProperties(); //property list for data entity

                entp.Add("unformated_String", s);
                entp.Add("formated_String", sf);
                entp.Add("delimiter", d);
                de.Properties = entp;

                dataEntities.Add(de);
            }

            return dataEntities;
        }
    }
}
