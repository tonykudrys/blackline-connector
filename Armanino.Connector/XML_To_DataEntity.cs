﻿using Scribe.Core.ConnectorApi;
using Scribe.Core.ConnectorApi.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Armanino.Connector
{
    class XML_To_DataEntity
    {
        public DataEntity XMLToEntity(String XML)
        {
            XDocument XDoc = XDocument.Parse(XML);
            String name = XDoc.Root.Name.ToString();
            DataEntity dataEnt = new DataEntity(name);  //new base data entity
            dataEnt.Children = new EntityChildren();    //child list in data entity
            dataEnt.Properties = new EntityProperties(); //property list for data entity
            
            foreach (XElement el in XDoc.Element(name).Elements())
            {
                String Value = el.Value.ToString();

                if (!el.HasElements)
                {
                    //is property
                    if (Value != null && Value != "")
                    {
                        var eptemp = getEntityPropert(el.Name.ToString(), Value);
                        dataEnt.Properties.Add(eptemp.First().Key, eptemp.First().Value);
                    }
                }
                else
                {
                    DataEntity child;
                    //is child
                    try
                    {
                        //yuky try catch here
                        child = XMLToEntity(el.Value);
                    }
                    catch(Exception ex)
                    {
                        child = XMLToEntity(el.ToString());
                    }

                    if(!dataEnt.Children.ContainsKey(name+"_"+el.Name.ToString()))
                    {
                        dataEnt.Children.Add(name + "_" + el.Name.ToString(), new List<DataEntity>());
                    }
                    
                    dataEnt.Children[name + "_" + el.Name.ToString()].Add(child);
                }
            }

            return dataEnt;
        }

        private EntityProperties getEntityPropert(String Name, string str)
        {
            EntityProperties ep = new EntityProperties();
            bool boolValue;
            Int32 intValue;
            Int64 bigintValue;
            double doubleValue;
            DateTime dateValue;

            // Place checks higher in if-else statement to give higher priority to type.

            if (bool.TryParse(str, out boolValue))
                ep.Add(Name, boolValue);
            else if (Int32.TryParse(str, out intValue))
                ep.Add(Name, intValue);
            else if (Int64.TryParse(str, out bigintValue))
                ep.Add(Name, bigintValue);
            else if (double.TryParse(str, out doubleValue))
                ep.Add(Name, doubleValue);
            else if (DateTime.TryParse(str, out dateValue))
                ep.Add(Name, dateValue);
            else ep.Add(Name, str);

            return ep;
        }

        private String getDataTypeStr(string str)
        {
            bool boolValue;
            Int32 intValue;
            Int64 bigintValue;
            double doubleValue;
            DateTime dateValue;

            // Place checks higher in if-else statement to give higher priority to type.

            if (bool.TryParse(str, out boolValue))
                return "Boolean";
            else if (Int32.TryParse(str, out intValue))
                return "Int32";
            else if (Int64.TryParse(str, out bigintValue))
                return "Int64";
            else if (double.TryParse(str, out doubleValue))
                return "Double";
            else if (DateTime.TryParse(str, out dateValue))
                return "DateTime";
            else return "String";

        }
    }
}
