﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Compilation;
using System.Xml.Linq;
using System.Xml.Serialization;
using ArmaninoObjModel;


namespace Armanino.Connector
{
    public class xml_to_obj
    {
        public T DeserializeFromXml<T>(string xml)
        {
            XDocument xInv = XDocument.Parse(xml);

            xInv.Descendants().Where(x => x.Value == "").Remove();

            T result;
            XmlSerializer ser = new XmlSerializer(typeof(T));
            using (TextReader tr = new StringReader(xInv.ToString()))
            {
                result = (T)ser.Deserialize(tr);
            }
            return result;
        }


        public String SerializeToXml<T>(T obj)
        {
            XmlSerializer serializer = new XmlSerializer(obj.GetType());

            using (StringWriter writer = new StringWriter())
            {
                serializer.Serialize(writer, obj);
                return writer.ToString();
            }
        }

        public List<con_objects> getTypeDic()
        {
            List<con_objects> typedic = new List<con_objects>();

            /*
            ArmaninoObjModel.obj_hold hold = new obj_hold();

            var otherAssembly = hold.GetType().Assembly;
            
            //var otherAssembly = hold.GetType().Assembly;
            var q = otherAssembly.GetTypes();

            

            foreach (Type t in q.ToList())
            {
                typedic.Add(new con_objects
                {
                    Name = t.Name,
                    type = t,
                    o = Activator.CreateInstance(t),
                });
            }
            */

            return typedic;
        }
    }

    public class con_objects
    {
        public String Name { get; set; }
        public Type type { get; set; }
        public object o { get; set; }

        public T getobj<T>(T t)
        {
            return (T)Activator.CreateInstance(t.GetType());
        }

    }
}


